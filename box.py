class Box :

    def __init__(self):
        self._content=[]
        self._ouvert=False
        self._capacity=None

    def add (self,x):
        self._content.append(x)
    
    def __contains__(self,truc):
        return truc in self._content

    def remove(self,x):
        self._content.remove(x)

    def is_open(self):
        return self._ouvert

    def open(self):
        self._ouvert = True

    def close(self):
        self._ouvert = False

    def action_look(self):
        if not self.is_open():
            return "la boite est fermee"
        else :
            return "la boite contient:"+','.join(self._content)
    
    def set_capacity(self,capacity):
        self._capacity=capacity

    def capacity(self):
        return self._capacity
    
    def has_room_for(self,truc):
        return self._capacity is None or self._capacity >= truc.getvolume()

    def __repr__(self):
        return "Thing("+str(self.getvolume())+","+self.getname()+")"
    def action_add(self,truc):
        if self._ouvert and self.has_room_for(truc):
            self.add(truc)
            if self.capacity() is not None:
                self._capacity = truc.getvolume()
            return True
        else:
            return False


class Chose :

    def __init__(self,volume,name):
        self._volume = volume
        self._name = name

    def getname(self):
        return self._name

    def getvolume(self):
        return self._volume

    def has_name(self,nom):
        return self.getname() == nom

    def has_room_for(self,volume):
        return self._capacity is None or self._capacity >= truc.volume()


    





